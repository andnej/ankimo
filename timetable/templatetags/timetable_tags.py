from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter(name='camelcapfirst')
@stringfilter
def camelcapfirst(value):
    return str.join(' ', map(lambda x: x[0].capitalize() + x[1:], value.split('_')))

@register.filter
def get_range_one_based( value ):
    """
    Filter - returns a list containing range made from given value
    Usage (in template):

    <ul>{% for i in 3|get_range %}
      <li>{{ i }}. Do something</li>
    {% endfor %}</ul>

    Results with the HTML:
    <ul>
      <li>0. Do something</li>
      <li>1. Do something</li>
      <li>2. Do something</li>
    </ul>

    Instead of 3 one may use the variable set in the views
    """
    return range( 1, value + 1 )