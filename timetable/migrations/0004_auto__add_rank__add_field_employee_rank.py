# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Rank'
        db.create_table('timetable_rank', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Institution'])),
        ))
        db.send_create_signal('timetable', ['Rank'])

        # Adding field 'Employee.rank'
        db.add_column('timetable_employee', 'rank',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Rank'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Rank'
        db.delete_table('timetable_rank')

        # Deleting field 'Employee.rank'
        db.delete_column('timetable_employee', 'rank_id')


    models = {
        'timetable.course': {
            'Meta': {'object_name': 'Course'},
            'credit': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_optional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'level': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'prerequisite': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Course']", 'null': 'True'}),
            'search_key': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'timetable.department': {
            'Meta': {'object_name': 'Department'},
            'faculty': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Faculty']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'timetable.employee': {
            'Meta': {'object_name': 'Employee'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'birth_city': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'birth_date': ('django.db.models.fields.DateField', [], {}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Institution']"}),
            'is_teacher': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'rank': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Rank']", 'null': 'True'})
        },
        'timetable.enrollment': {
            'Meta': {'object_name': 'Enrollment'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Course']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lesson': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Lesson']", 'null': 'True'}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Period']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Student']"})
        },
        'timetable.faculty': {
            'Meta': {'object_name': 'Faculty'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Institution']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'timetable.guardian': {
            'Meta': {'object_name': 'Guardian'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'birth_city': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'birth_date': ('django.db.models.fields.DateField', [], {}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Student']"})
        },
        'timetable.institution': {
            'Meta': {'object_name': 'Institution'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'timetable.lesson': {
            'Meta': {'object_name': 'Lesson'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Course']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Employee']"})
        },
        'timetable.period': {
            'Meta': {'object_name': 'Period'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_open': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        'timetable.rank': {
            'Meta': {'object_name': 'Rank'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Institution']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'timetable.replacementschedule': {
            'Meta': {'object_name': 'ReplacementSchedule'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'replacement_for': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Schedule']"}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Room']"}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        'timetable.room': {
            'Meta': {'object_name': 'Room'},
            'capacity': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_activity': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_classroom': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'managed_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Faculty']"}),
            'search_key': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'timetable.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lesson': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Lesson']"}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Room']"}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        'timetable.student': {
            'Meta': {'object_name': 'Student'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'birth_city': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'birth_date': ('django.db.models.fields.DateField', [], {}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Department']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'register': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'})
        }
    }

    complete_apps = ['timetable']