# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Institution'
        db.create_table('timetable_institution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=120)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('province', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('timetable', ['Institution'])

        # Adding model 'Faculty'
        db.create_table('timetable_faculty', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=120)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('province', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Institution'])),
        ))
        db.send_create_signal('timetable', ['Faculty'])

        # Adding model 'Department'
        db.create_table('timetable_department', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('faculty', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Faculty'])),
        ))
        db.send_create_signal('timetable', ['Department'])

        # Adding model 'Employee'
        db.create_table('timetable_employee', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('birth_date', self.gf('django.db.models.fields.DateField')()),
            ('birth_city', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=120)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('mobile', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('is_teacher', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Institution'])),
        ))
        db.send_create_signal('timetable', ['Employee'])

        # Adding model 'Student'
        db.create_table('timetable_student', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('birth_date', self.gf('django.db.models.fields.DateField')()),
            ('birth_city', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=120)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('mobile', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Department'])),
        ))
        db.send_create_signal('timetable', ['Student'])

        # Adding model 'Guardian'
        db.create_table('timetable_guardian', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('birth_date', self.gf('django.db.models.fields.DateField')()),
            ('birth_city', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('address', self.gf('django.db.models.fields.TextField')(max_length=120)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('mobile', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Student'])),
        ))
        db.send_create_signal('timetable', ['Guardian'])

        # Adding model 'Room'
        db.create_table('timetable_room', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('search_key', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('capacity', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('is_activity', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_classroom', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('managed_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Faculty'])),
        ))
        db.send_create_signal('timetable', ['Room'])

        # Adding model 'Course'
        db.create_table('timetable_course', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('search_key', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('prerequisite', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Course'], null=True)),
            ('credit', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('level', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('is_optional', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('timetable', ['Course'])

        # Adding model 'Period'
        db.create_table('timetable_period', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('is_open', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('timetable', ['Period'])

        # Adding model 'Lesson'
        db.create_table('timetable_lesson', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Course'])),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Employee'])),
        ))
        db.send_create_signal('timetable', ['Lesson'])

        # Adding model 'Schedule'
        db.create_table('timetable_schedule', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('day', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('start_time', self.gf('django.db.models.fields.TimeField')()),
            ('end_time', self.gf('django.db.models.fields.TimeField')()),
            ('lesson', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Lesson'])),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Room'])),
        ))
        db.send_create_signal('timetable', ['Schedule'])

        # Adding model 'ReplacementSchedule'
        db.create_table('timetable_replacementschedule', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('start_time', self.gf('django.db.models.fields.TimeField')()),
            ('end_time', self.gf('django.db.models.fields.TimeField')()),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Room'])),
            ('replacement_for', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Schedule'])),
        ))
        db.send_create_signal('timetable', ['ReplacementSchedule'])

        # Adding model 'Enrollment'
        db.create_table('timetable_enrollment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('period', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Period'])),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Student'])),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Course'])),
            ('lesson', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['timetable.Lesson'], null=True)),
        ))
        db.send_create_signal('timetable', ['Enrollment'])


    def backwards(self, orm):
        # Deleting model 'Institution'
        db.delete_table('timetable_institution')

        # Deleting model 'Faculty'
        db.delete_table('timetable_faculty')

        # Deleting model 'Department'
        db.delete_table('timetable_department')

        # Deleting model 'Employee'
        db.delete_table('timetable_employee')

        # Deleting model 'Student'
        db.delete_table('timetable_student')

        # Deleting model 'Guardian'
        db.delete_table('timetable_guardian')

        # Deleting model 'Room'
        db.delete_table('timetable_room')

        # Deleting model 'Course'
        db.delete_table('timetable_course')

        # Deleting model 'Period'
        db.delete_table('timetable_period')

        # Deleting model 'Lesson'
        db.delete_table('timetable_lesson')

        # Deleting model 'Schedule'
        db.delete_table('timetable_schedule')

        # Deleting model 'ReplacementSchedule'
        db.delete_table('timetable_replacementschedule')

        # Deleting model 'Enrollment'
        db.delete_table('timetable_enrollment')


    models = {
        'timetable.course': {
            'Meta': {'object_name': 'Course'},
            'credit': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_optional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'level': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'prerequisite': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Course']", 'null': 'True'}),
            'search_key': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'timetable.department': {
            'Meta': {'object_name': 'Department'},
            'faculty': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Faculty']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'timetable.employee': {
            'Meta': {'object_name': 'Employee'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'birth_city': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'birth_date': ('django.db.models.fields.DateField', [], {}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Institution']"}),
            'is_teacher': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'timetable.enrollment': {
            'Meta': {'object_name': 'Enrollment'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Course']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lesson': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Lesson']", 'null': 'True'}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Period']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Student']"})
        },
        'timetable.faculty': {
            'Meta': {'object_name': 'Faculty'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Institution']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'timetable.guardian': {
            'Meta': {'object_name': 'Guardian'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'birth_city': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'birth_date': ('django.db.models.fields.DateField', [], {}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Student']"})
        },
        'timetable.institution': {
            'Meta': {'object_name': 'Institution'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'province': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'timetable.lesson': {
            'Meta': {'object_name': 'Lesson'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Course']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Employee']"})
        },
        'timetable.period': {
            'Meta': {'object_name': 'Period'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_open': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        'timetable.replacementschedule': {
            'Meta': {'object_name': 'ReplacementSchedule'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'replacement_for': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Schedule']"}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Room']"}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        'timetable.room': {
            'Meta': {'object_name': 'Room'},
            'capacity': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_activity': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_classroom': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'managed_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Faculty']"}),
            'search_key': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'timetable.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'day': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lesson': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Lesson']"}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Room']"}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        'timetable.student': {
            'Meta': {'object_name': 'Student'},
            'address': ('django.db.models.fields.TextField', [], {'max_length': '120'}),
            'birth_city': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'birth_date': ('django.db.models.fields.DateField', [], {}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['timetable.Department']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['timetable']