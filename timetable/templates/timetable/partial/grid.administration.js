{% extends "timetable/partial/navigation.administration.js" %}
{% block onselect %}
    jQuery('#{{module}}_edit').attr("disabled", false);
{% endblock %}
{% block buttons %}
{% autoescape off %}
    jQuery('#{{module}}_new').on("click", function(event){
        var ppk = jQuery('#{{module}}_parent').attr('value');
        jQuery('#{{module}}_selected').attr('value', '-1');
        jQuery('#{{module}}_edit').attr('disabled', 'true');
        disable_all_view(true);
        jQuery("#{{module}}").append('<div id="{{module}}_dialog">&nbsp;</div>');
        Dajaxice.timetable.{{module}}_new(Dajax.process, {'ppk':ppk});
    })
    jQuery('#{{module}}_edit').on("click", function(event){
        var ppk = jQuery('#{{module}}_parent').attr('value');
        var pk = jQuery('#{{module}}_selected').attr('value');
        jQuery("#{{module}}").append('<div id="{{module}}_dialog">&nbsp;</div>');
        Dajaxice.timetable.{{module}}_edit(Dajax.process, {'pk': pk, 'ppk':ppk});
    })
{% endautoescape %}
{% endblock %}
