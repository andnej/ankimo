jQuery('.bsmListItem').draggable({
    snap:true, 
    scroll:false,
    revert: function(event) {
        if (event) {
            var level = jQuery(event).attr("id").slice(6);
            var courseId = jQuery(this).attr("id").slice(7);
            Dajaxice.timetable.course_level_drag(function(data) {
                var ppk = jQuery('#course_level_parent').attr("value");
                var current_max_level = parseInt(jQuery('#max_level').attr("value"));
                console.log("current max level:%d", current_max_level);
                var new_max_level = data.max_level;
                var max_level = new_max_level > current_max_level ? new_max_level : current_max_level;
                console.log('new_max_level:%d', new_max_level);
                console.log("using max_level:%d", max_level);
                jQuery('#max_level').attr("value", max_level);
                var diff = max_level - current_max_level;
                console.log("diff:%d", diff);
                for (i = 1; i <= diff; i++) {
                    console.log("adding level %d", current_max_level + i);
                    Dajaxice.timetable.course_level_add(Dajax.process, {'level':current_max_level + i});
                }
                for (i = 1; i <= max_level; i++) {
                    Dajaxice.timetable.course_level_update(Dajax.process, { 'level':i, 'ppk':ppk} );
                }
            }, {'source':courseId, 'level':level})
        }
        
        return !event;
    }
});
jQuery(".levelBox").droppable();
