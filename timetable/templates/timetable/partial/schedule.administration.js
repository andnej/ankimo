{% extends "timetable/partial/navigation.administration.js" %}
{% block content %}
    jQuery('#{{module}}_content').fullCalendar({
        eventSources: [
            {url:'{{url}}', color:'blue'}
        ],
        defaultView: 'agendaWeek',
        height: '325',
        theme: true,
        minTime: '6:00',
        allDaySlot: false,
        header: {
            left: '',
            center: '',
            right: ''
        },
        columnFormat: {
            month: 'ddd',
            week: 'ddd',
            day: 'dddd M/d'
        },
        timeFormat: {
          agenda: '',
          '': 'H(:mm)'  
        },
        axisFormat: 'H:mm',
        eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
            Dajaxice.timetable.schedule_drop(Dajax.process, {'pk':event.id, 'day':dayDelta, 'minute':minuteDelta})
        },
        eventResize: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
            if (dayDelta != 0) {
                revertFunc();
            } else {
                Dajaxice.timetable.schedule_resize(Dajax.process, {'pk':event.id, 'minute':minuteDelta});
            }
        },
        eventClick: function(event) {
            if (event.editable) {
                onSelect(event.id);
            }
        }
    });
{% endblock %}
