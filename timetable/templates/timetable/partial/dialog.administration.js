{% load timetable_tags %}
{% autoescape off %}
	jQuery('#{{module}}_dialog')
		.dialog({
			title: '{{module|camelcapfirst}}',
			modal: true,
			buttons: {
				"Save": function() {
				    pk = jQuery('#{{module}}_selected').attr("value");
					data = jQuery('#{{module}}_form').serializeObject();
					Dajaxice.timetable.{{module}}_save(Dajax.process, {'form':data, 'pk': pk});
				},
				"Cancel": function() {
				    jQuery(this).dialog("destroy");
				    jQuery(this).remove();
				}
			}
		})
{% endautoescape %}
