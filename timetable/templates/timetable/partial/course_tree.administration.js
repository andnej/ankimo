{% extends "timetable/partial/navigation.administration.js" %}
{% load staticfiles %}
{% block content %}
$("#{{module}}_pager").dynatree({
    initAjax : {
        url : "{{url}}",
        data : {
            pk : 0
        }
    },
    onLazyRead : function(node) {
        node.appendAjax({
            url : "{{url}}",
            data : {
                pk : node.data.key
            }
        })
    },
    dnd : {
        onDragStart : function(node) {
            /** This function MUST be defined to enable dragging for the tree.
             *  Return false to cancel dragging of node.
             */
            logMsg("tree.onDragStart(%o)", node);
            return true;
        },
        onDragEnter: function(node, sourceNode) {
            return ["before", "after", "over"]
        },
        onDrop : function(node, sourceNode, hitMode, ui, draggable) {
            /** This function MUST be defined to enable dropping of items on
             * the tree.
             */
            logMsg("tree.onDrop(%o, %o, %s)", node, sourceNode, hitMode);
            sourceNode.move(node, hitMode);
            Dajaxice.timetable.course_drop(Dajax.process, {
                'target':node.data.key, 
                'source':sourceNode.data.key, 
                'hitmode':hitMode})
        }
    }
});
{% endblock %}
