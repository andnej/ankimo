{% for id in ids %}
jQuery("#{{id}}")
    .datepicker({
        changeMonth:true, 
        changeYear:true,
        dateFormat:'yy-mm-dd'});
{% endfor %}

{% if start_time and end_time %}
jQuery('#{{start_time}}').timeEntry({show24Hours: true, spinnerImage: '', timeSteps:[0,30], showSeconds:false})
jQuery('#{{end_time}}').timeEntry({show24Hours: true, spinnerImage: '', timeSteps:[0,30], showSeconds:false})
{% endif %}
