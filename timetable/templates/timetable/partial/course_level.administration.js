{% load timetable_tags %}
jQuery('#faculty').addClass('hidden');
jQuery('#add_level').on("click", function() {
    var max_level = parseInt(jQuery('#max_level').attr("value"));
    var levelToAdd = max_level + 1;
    Dajaxice.timetable.course_level_add(Dajax.process, {'level':levelToAdd})
})
jQuery('#remove_level').on("click", function() {
    var max_level = parseInt(jQuery('#max_level').attr("value"));
    var last_level_content = jQuery('#level_' + max_level + " ol li");
    if (last_level_content.length == 0) {
        var last_level = jQuery('#level_' + max_level);
        last_level.remove();
        jQuery('#max_level').attr("value", max_level - 1);
    }
})
{% for level in max_level|get_range_one_based %}
Dajaxice.timetable.course_level_update(Dajax.process, { 'level':{{level}}, 'ppk':{{ppk}} })
{% endfor %}
