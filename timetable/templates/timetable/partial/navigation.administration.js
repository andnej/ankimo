{% load timetable_tags %}
{% autoescape off %}
    
    function hide_all_next_module() {
        {% for next_module in next_module_list %}
            jQuery('#{{next_module}}').addClass('hidden');
        {% endfor %}
    }
    {% for next_module in next_module_list %}
        jQuery('#view_{{next_module}}').on("click", function(event) {
            var gpk = jQuery('#{{module}}_parent').attr('value');
            var ppk = jQuery('#{{module}}_selected').attr('value');
            Dajaxice.timetable.{{next_module}}_grid(Dajax.process, {'ppk': ppk, 'gpk': gpk, 'prev_module': '{{module}}'});
            hide_all_next_module();
            jQuery('#{{next_module}}').removeClass('hidden');
        })
    {% endfor %}
    {% if hidden_module%}
        jQuery('#{{hidden_module}}').addClass('hidden');
    {% endif %}
    function disable_all_view(disabled) {
        jQuery('#{{module}} > center > button.row_controlled').attr("disabled", disabled)
    }
    
    function onSelect(id) {
        disable_all_view(false);
        {% block onselect %} {% endblock %}
        jQuery('#{{module}}_selected').attr("value", id);
    }
    
    {% block content %}
    jQuery('#{{module}}_table')
        .jqGrid({
            url: '{{url}}',
            datatype: 'json',
            colNames: {{col_names}},
            colModel: {{col_model}},
            rowNum: 10,
            rowList: [10,20,30],
            pager: '{{module}}_pager',
            viewrecords: true,
            sortorder: 'asc',
            caption: '{{module|camelcapfirst}}',
            width: '100%',
            height: '100%',
            shrinkToFit: true,
            autowidth: true,
            {% if default_module %}
            ondblClickRow: function(id) {
                var gpk = jQuery('#{{module}}_parent').attr('value');
                var ppk = jQuery('#{{module}}_selected').attr('value');
                Dajaxice.timetable.{{default_module}}_grid(Dajax.process, {'ppk': ppk, 'gpk': gpk, 'prev_module': '{{module}}'});
                hide_all_next_module();
                jQuery('#{{default_module}}').removeClass('hidden');
            },
            {% endif %}
            onSelectRow: onSelect
        }).navGrid("{{module}}_pager",{edit:false, add:false, del:false});
    {% endblock %}
    {% block buttons %}
    {% endblock %}
{% endautoescape %}
