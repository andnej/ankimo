from timetable import models
from django.http import HttpResponse
from django.db.models import Count
from math import ceil
from datetime import datetime, date, timedelta
import logging
import simplejson as json

logger = logging.getLogger('timetable')

def generic_jqgrid(request, queryset, to_rows):
    page = int(request.GET['page'])
    limit = int(request.GET['rows'])
    sidx = request.GET['sidx']
    sord = request.GET['sord']
    prefix = '' if sord == 'asc' else '-'
    if (sidx and sord):
        queryset = queryset.order_by('%s%s' % (prefix, sidx))
    count = queryset.count()
    total_pages = 1
    
    if count > 0:
        total_pages = int(ceil(float(count) / limit)) 
    
    if page > total_pages:
        page = total_pages
    start = limit * page - limit
    end = start + limit
    logger.info("fetching data start:%i, end:%i" % (start, end))
    results = queryset[start:end]
    rows = to_rows(results)
    
    to_json = {
        'page': page, 
        'total': total_pages,
        'records': count,
        'rows': rows,
    }
    #json = JSONSerializer()
    return HttpResponse(json.dumps(to_json), mimetype='application/json')

def institution_all(request):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.name, row.phone_number, row.city]})
        return rows
    queryset = models.Institution.objects.all()
    return generic_jqgrid(request, queryset, to_rows)

def faculty_all(request, ppk):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.name, row.phone_number, row.address]})
        return rows
    queryset = models.Faculty.objects.filter(institution=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def department_all(request, ppk):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.name]})
        return rows
    queryset = models.Department.objects.filter(faculty=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def rank_all(request, ppk):
    def to_rows(results):
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.name]})
        return rows
    queryset = models.Rank.objects.filter(institution=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def employee_all(request, ppk):
    def to_rows(results):
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.first_name, row.last_name, row.mobile]})
        return rows
    queryset = models.Employee.objects.filter(institution=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def student_all(request, ppk):
    def to_rows(results):
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.register, row.first_name, row.mobile]})
        return rows
    queryset = models.Student.objects.filter(department=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def guardian_all(request, ppk):
    def to_rows(results):
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.first_name, row.last_name, row.mobile]})
        return rows
    queryset = models.Guardian.objects.filter(student=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def room_all(request, ppk):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.search_key, row.capacity, row.is_classroom, row.is_activity]})
        return rows
    queryset = models.Room.objects.filter(managed_by=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def course_all(request, ppk):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.search_key, row.name, row.credit, row.level, row.is_optional]})
        return rows
    queryset = models.Course.objects.filter(department=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def period_all(request, ppk):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.name, row.start_date.isoformat(), row.end_date.isoformat(), row.is_open]})
        return rows
    queryset = models.Period.objects.filter(institution=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def enrollment_all(request, ppk):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.period.__unicode__(), row.course.__unicode__(), row.student.__unicode__()]})
        return rows
    queryset = models.Enrollment.objects.filter(period=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def enrollment_statistic_all(request, ppk):
    def to_rows(results):    
        rows = []
        for row in results:
            rows.append({'id': row['course__search_key'], 'cell':[row['student__department__name'], "%s %s" % (row['course__search_key'], row['course__name']), row['course_count'], row['allocated']]})
        return rows
    queryset = models.Enrollment.objects.filter(period=ppk)
    queryset = queryset.filter(course__department=request.session["department"])
    queryset = queryset.values('student__department__name', 'course__search_key', 'course__name')
    queryset = queryset.annotate(course_count=Count('id'))
    queryset = queryset.annotate(allocated=Count('lesson'))
    return generic_jqgrid(request, queryset, to_rows)

def enrollment_class_all(request, ppk, gpk):
    #ppk is course id, gpk is period id
    def to_rows(results):
        rows = []
        for row in results:
            rows.append({'id': row.id, 'cell':[row.id, row.name, row.period.__unicode__(), row.course.__unicode__(), row.teacher.__unicode__()]})
        return rows
    queryset = models.Class.objects.filter(period=gpk).filter(course__search_key=ppk)
    return generic_jqgrid(request, queryset, to_rows)

def schedule_all(request, ppk):
    #ppk is class id
    start = request.GET["start"]
    end = request.GET["end"]
    start = date.fromtimestamp(int(start)) #start is always a monday
    end = date.fromtimestamp((int(end)))
    clazz = models.Class.objects.get(pk=ppk)
    period = clazz.period
    room_id = request.session["room"]
    room = models.Room.objects.get(pk=room_id)
    schedules = models.Schedule.objects.filter(room=room).filter(lesson__period=period)
    to_json = []
    
    for schedule in schedules:
        schedule_date = start + timedelta(days=schedule.day)
        schedule_start = datetime.combine(schedule_date, schedule.start_time)
        schedule_end = datetime.combine(schedule_date, schedule.end_time)
        #editable = schedule.lesson == clazz
        editable = True
        color = 'blue' if editable else 'red'
        to_json.append({
            'id':schedule.id, 
            'title': "%s %s" % (schedule.lesson.course.name, schedule.lesson.name),
            'start':schedule_start.isoformat(),
            'end':schedule_end.isoformat(),
            'allDay':False,
            'editable':editable,
            'color':color
            })
    return HttpResponse(json.dumps(to_json), mimetype='application/json')

def student_schedule_all(request, ppk):
    #ppk is period id
    start = request.GET["start"]
    end = request.GET["end"]
    start = date.fromtimestamp(int(start)) #start is always a monday
    end = date.fromtimestamp((int(end)))
    student = request.session["student"]
    schset = models.Schedule.objects.filter(lesson__period=ppk)
    schset = schset.filter(lesson__enrollment__student=student)
    to_json = []
    for schedule in schset:
        schedule_date = start + timedelta(days=schedule.day)
        schedule_start = datetime.combine(schedule_date, schedule.start_time)
        schedule_end = datetime.combine(schedule_date, schedule.end_time)
        editable = False
        color = 'blue' if editable else 'red'
        to_json.append({
            'id':schedule.id, 
            'title': "%s %s %s" % (schedule.room.search_key, schedule.lesson.course.name, schedule.lesson.name),
            'start':schedule_start.isoformat(),
            'end':schedule_end.isoformat(),
            'allDay':False,
            'editable':editable,
            'color':color
            })
    return HttpResponse(json.dumps(to_json), mimetype='application/json')
    
def course_tree(request, ppk):
    pk = int(request.GET["pk"]) 
    department = models.Department.objects.get(pk=ppk)
    def course_to_json(course):
        return {"title":course.__unicode__(), "isLazy": True, "key":course.id}
    if pk > 0:
        course = models.Course.objects.get(pk=pk)
        courses = department.course_set.filter(prerequisite=course).order_by("search_key")
        children = map(course_to_json, courses)
    else:
        courses = department.course_set.filter(prerequisite=None).order_by("search_key")
        children = map(course_to_json, courses)
    return HttpResponse(json.dumps(children), mimetype='application/json')
