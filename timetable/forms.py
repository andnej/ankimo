from django.forms import ModelForm
from timetable import models

class InstitutionForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Institution
        
class FacultyForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Faculty
        
class DepartmentForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Department

class RankForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Rank

class EmployeeForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Employee

class StudentForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Student
        
class GuardianForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Guardian
    
class RoomForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Room

class CourseForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Course
    

class PeriodForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Period
        
class EnrollmentForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Enrollment
        fields = ['period', 'course', 'student']

class ClassForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Class

class ScheduleForm(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Schedule

class ScheduleEditor(ModelForm):
    error_css_class = "error"
    required_css_class = "required"
    
    class Meta:
        model = models.Schedule
        fields = ['day', 'start_time', 'end_time', 'room']