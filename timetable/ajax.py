from django.template.loader import get_template
from django.template import Context
from django.utils import simplejson
from django.db.models import Max, Count, Min
from dajaxice.decorators import dajaxice_register
from dajax.core import Dajax
from StringIO import StringIO
from timetable import forms, models
from datetime import date, time, timedelta, datetime
from constraint import Problem
import random
import logging
import simplejson as json

logger = logging.getLogger(__name__)

@dajaxice_register
def dajaxice_example(request, text):
    return simplejson.dumps({'message':'you send %s!' % text})

@dajaxice_register
def randomize(request):
    dajax = Dajax()
    dajax.assign("#text", 'value', random.randint(1, 10))
    return dajax.json()

def breadcrumbs(session, prev_module, module):
    crumbs = session.get("crumbs", [])
    if (prev_module):
        try:
            i = crumbs.index(prev_module)
            logger.info("prev_module:%s, index:%d" % (prev_module, i))
            crumbs = crumbs[:(i + 1)]
            crumbs.append(module)
        except ValueError:
            crumbs.append(module)
    else:
        crumbs = [module,]
    session["crumbs"] = crumbs
    return crumbs

def generic_grid(crumbs, module, next_module_list, default_module, 
                 col_names, col_model, url, 
                 hidden_module=None, prev_module=None, ppk=-1, 
                 html="timetable/partial/grid.administration.html", 
                 js="timetable/partial/grid.administration.js",
                 extra_buttons=[], extra_hiddens=[], extra_controls=[]):
    col_names_io = StringIO()
    simplejson.dump(col_names, col_names_io)
    col_model_io = StringIO()
    simplejson.dump(col_model, col_model_io)
    logger.info("dumped: \n%s\n%s" % (col_names_io.getvalue(), col_model_io.getvalue()))
    logger.info("module:%s, prev module:%s, crumbs: %s" % (module, prev_module, ', '.join(crumbs)))
    t = get_template(html)
    html = t.render(Context({
        'module': module, 
        'next_module_list': next_module_list, 
        'ppk':ppk, 
        'crumbs': crumbs,
        'extra_buttons': extra_buttons,
        'extra_hiddens': extra_hiddens,
        'extra_controls': extra_controls}))
    t = get_template(js)
    js = t.render(Context({
        'module': module,
        'url': url, 
        'col_names':col_names_io.getvalue(),
        'col_model':col_model_io.getvalue(),
        'next_module_list': next_module_list,
        'default_module': default_module,
        'hidden_module': hidden_module,
        'prev_module': prev_module,
        'ppk': ppk}))
    logger.info("returned html:%s" % html)
    
    t = get_template('timetable/partial/crumbs.administration.html')
    breadcrumbs = t.render(Context({'crumbs': crumbs}))
    
    dajax = Dajax()
    dajax.assign("#%s" % module, "innerHTML", html)
    dajax.script(js)
    dajax.assign("#breadcrumbs", "innerHTML", breadcrumbs)
    
    return dajax.json()

def generic_form(module, form, after_load=None):
    t = get_template("timetable/partial/dialog.administration.html")
    html = t.render(Context({'form': form, 'module': module}))
    t = get_template("timetable/partial/dialog.administration.js")
    js = t.render(Context({'module': module}))
    
    dajax = Dajax()
    dajax.assign("#%s_dialog" % module, "innerHTML", html)
    dajax.script(js)
    dajax.add_css_class("p > :input", "text ui-widget-content ui-corner-all")
    if after_load:
        dajax.script(after_load)
    return dajax.json()

def generic_save(module, ModelClass, ModelForm, pk, form, after_save_script=None):
    logger.info("form: %s" % json.dumps(form))
    logger.info("pk: %s" % pk)
    
    if int(pk) > -1:
        logger.info("pk is larger than -1, update mode")
        theObject = ModelClass.objects.get(pk=pk)
        form = ModelForm(form, instance=theObject)
    else:
        logger.info("pk is -1, insert mode")
        form = ModelForm(form)
    
    dajax = Dajax()
    if form.is_valid():
        form.save()
        dajax.script("jQuery('#%s_dialog').dialog('destroy')" % module)
        dajax.script("jQuery('#%s_dialog').remove()" % module)
        dajax.script("jQuery('#%s_table').trigger('reloadGrid')" % module)
        dajax.script("jQuery('button.row_controlled').attr('disabled', true)")
        if after_save_script:
            dajax.script(after_save_script)
    else:
        dajax.remove_css_class("p > :input", "error")
        for error in form.errors:
            dajax.add_css_class("#id_%s" % error, "error")
        if form.errors.has_key('__all__') or form.errors.has_key(module):
            dajax.assign("#%s_errors" % module, "innerHTML", form.non_field_errors())
            dajax.remove_css_class("#%s_errors" % module, "hidden")
        else:
            dajax.add_css_class("#%s_errors" % module, "hidden")
        
    return dajax.json()

@dajaxice_register
def breadcrumbs_show(request, module):
    crumbs = request.session.get("crumbs", [])
    dajax = Dajax()
    to_show = []
    try:
        i = crumbs.index(module)
        to_show.append(module)
        if i == (len(crumbs) - 1):
            #last item
            if (len(crumbs) > 1):
                prev = crumbs[i - 1]
                to_show.append(prev)
            #do no more on 1st item
        else:
            if i < len(crumbs) - 1:
                #has next
                nxt = crumbs[i + 1]
                to_show.append(nxt)
        for crumb in crumbs:
            if crumb in to_show:
                dajax.remove_css_class("#%s" % crumb, "hidden")
            else:
                dajax.add_css_class("#%s" % crumb, "hidden")
    except ValueError:
        pass
    return dajax.json()

@dajaxice_register    
def institution_grid(request):
    module = "institution"
    crumbs = breadcrumbs(request.session, None, module)
    next_module_list = ('faculty', 'rank', 'employee', 'period')
    default_module = 'faculty'
    col_names = ['Id', 'Name', 'Phone', 'City']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'name', 'index':'name', 'width': '175px'},
        {'name': 'phone_number', 'index': 'phone_number', 'width': '125px'},
        {'name': 'city', 'index':'city', 'width': '75px'}
    ]
    url = '/json/institution/'
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url)

@dajaxice_register    
def institution_new(request, ppk):
    module = "institution"
    form = forms.InstitutionForm()
    
    return generic_form(module, form)

@dajaxice_register    
def institution_edit(request, pk, ppk):
    module = "institution"
    institution = models.Institution.objects.get(pk=pk)
    form = forms.InstitutionForm(instance=institution)
    
    return generic_form(module, form)

@dajaxice_register
def institution_save(request, form, pk):
    return generic_save('institution', models.Institution, forms.InstitutionForm, pk, form)

@dajaxice_register    
def faculty_grid(request, ppk, gpk, prev_module):
    module = "faculty"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ('department', 'room',)
    default_module = 'department'
    col_names = ['Id', 'Name', 'Phone', 'Address']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'name', 'index':'name', 'width': '150px'},
        {'name': 'phone_number', 'index': 'phone_number', 'width': '75px'},
        {'name': 'address', 'index': 'address', 'width':'150px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        ppk=ppk, prev_module=prev_module)

@dajaxice_register    
def faculty_new(request, ppk):
    module = "faculty"
    form = forms.FacultyForm(initial={'institution':models.Institution.objects.get(pk=ppk)})
    form.fields["institution"].queryset = models.Institution.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def faculty_edit(request, pk, ppk):
    module = "faculty"
    faculty = models.Faculty.objects.get(pk=pk)
    form = forms.FacultyForm(instance=faculty)
    form.fields["institution"].queryset = models.Institution.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def faculty_save(request, form, pk):
    return generic_save('faculty', models.Faculty, forms.FacultyForm, pk, form)

@dajaxice_register    
def department_grid(request, ppk, gpk, prev_module):
    module = "department"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ('student', 'course', 'course_tree', 'course_level')
    default_module = 'student'
    col_names = ['Id', 'Name',]
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'name', 'index':'name', 'width': '375px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        hidden_module="institution", prev_module=prev_module, ppk=ppk)

@dajaxice_register    
def department_new(request, ppk):
    module = "department"
    form = forms.DepartmentForm(initial={'faculty': models.Faculty.objects.get(pk=ppk)})
    form.fields["faculty"].queryset = models.Faculty.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def department_edit(request, pk, ppk):
    module = "department"
    department = models.Department.objects.get(pk=pk)
    form = forms.DepartmentForm(instance=department)
    form.fields["faculty"].queryset = models.Faculty.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def department_save(request, form, pk):
    return generic_save('department', models.Department, forms.DepartmentForm, pk, form)

@dajaxice_register    
def rank_grid(request, ppk, gpk, prev_module):
    module = "rank"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    col_names = ['Id', 'Name',]
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'name', 'index':'name', 'width': '375px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        ppk=ppk, prev_module=prev_module)

@dajaxice_register    
def rank_new(request, ppk):
    module = "rank"
    form = forms.RankForm(initial={'institution':models.Institution.objects.get(pk=ppk)})
    form.fields['institution'].queryset = models.Institution.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def rank_edit(request, pk, ppk):
    module = "rank"
    rank = models.Rank.objects.get(pk=pk)
    form = forms.RankForm(instance=rank)
    form.fields['institution'].queryset = models.Institution.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def rank_save(request, form, pk):
    return generic_save('rank', models.Rank, forms.RankForm, pk, form)

@dajaxice_register    
def employee_grid(request, ppk, gpk, prev_module):
    module = "employee"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    col_names = ['Id', 'First Name', 'Last Name', 'Mobile']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'first_name', 'index':'first_name', 'width': '125px'},
        {'name': 'last_name', 'index':'last_name', 'width': '125px'},
        {'name': 'mobile', 'index':'mobile', 'width': '125px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        ppk=ppk, prev_module=prev_module)

@dajaxice_register    
def employee_new(request, ppk):
    module = "employee"
    institution = models.Institution.objects.get(pk=ppk)
    form = forms.EmployeeForm(initial={'birth_date':date(1970,1,1), 'institution':institution})
    form.fields['institution'].queryset = models.Institution.objects.filter(id=ppk)
    form.fields['rank'].queryset = models.Rank.objects.filter(institution=institution)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_birth_date']})))

@dajaxice_register    
def employee_edit(request, pk, ppk):
    module = "employee"
    institution = models.Institution.objects.get(pk=ppk)
    employee = models.Employee.objects.get(pk=pk)
    form = forms.EmployeeForm(instance=employee)
    form.fields['institution'].queryset = models.Institution.objects.filter(id=ppk)
    form.fields['rank'].queryset = models.Rank.objects.filter(institution=institution)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_birth_date']})))

@dajaxice_register    
def employee_save(request, form, pk):
    return generic_save('employee', models.Employee, forms.EmployeeForm, pk, form)

@dajaxice_register    
def student_grid(request, ppk, gpk, prev_module):
    module = "student"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ('guardian',)
    default_module = 'guardian'
    col_names = ['Id', 'Register', 'First Name', 'Mobile']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'register', 'index':'register', 'width': '125px'},
        {'name': 'first_name', 'index':'first_name', 'width': '125px'},
        {'name': 'mobile', 'index':'mobile', 'width':'125px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url,
        hidden_module="faculty", prev_module=prev_module, ppk=ppk)

@dajaxice_register    
def student_new(request, ppk):
    module = "student"
    department = models.Department.objects.get(pk=ppk)
    form = forms.StudentForm(initial={'birth_date':date(1970,1,1), 'department': department})
    form.fields['department'].queryset = models.Department.objects.filter(id=ppk)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_birth_date']})))

@dajaxice_register    
def student_edit(request, pk, ppk):
    module = "student"
    student = models.Student.objects.get(pk=pk)
    form = forms.StudentForm(instance=student)
    form.fields['department'].queryset = models.Department.objects.filter(id=ppk)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_birth_date']})))

@dajaxice_register    
def student_save(request, form, pk):
    return generic_save('student', models.Student, forms.StudentForm, pk, form)

@dajaxice_register    
def guardian_grid(request, ppk, gpk, prev_module):
    module = "guardian"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    col_names = ['Id', 'First Name', 'Last Name', 'Mobile']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'first_name', 'index':'first_name', 'width': '125px'},
        {'name': 'last_name', 'index':'last_name', 'width': '125px'},
        {'name': 'mobile', 'index':'mobile', 'width': '125px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url,
        hidden_module="department", prev_module=prev_module, ppk=ppk)

@dajaxice_register    
def guardian_new(request, ppk):
    module = "guardian"
    student = models.Student.objects.get(pk=ppk)
    form = forms.GuardianForm(initial={'birth_date':date(1970,1,1), 'student': student})
    form.fields['student'].queryset = models.Student.objects.filter(id=ppk)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_birth_date']})))

@dajaxice_register    
def guardian_edit(request, pk, ppk):
    module = "guardian"
    guardian = models.Guardian.objects.get(pk=pk)
    form = forms.GuardianForm(instance=guardian)
    form.fields['student'].queryset = models.Student.objects.filter(id=ppk)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_birth_date']})))

@dajaxice_register    
def guardian_save(request, form, pk):
    return generic_save('guardian', models.Guardian, forms.GuardianForm, pk, form)

@dajaxice_register    
def room_grid(request, ppk, gpk, prev_module):
    module = "room"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    col_names = ['Id', 'Search Key', 'Capacity', 'ClassRoom', 'Activity']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'search_key', 'index':'search_key', 'align': 'right', 'width': '125px'},
        {'name': 'capacity', 'index':'capacity', 'width': '100px', 'align': 'right', 'width': '100px'},
        {'name': 'is_classroom', 'index': 'is_classroom', 'width': '75px', 'formatter': 'checkbox', 'width': '75px'},
        {'name': 'is_activity', 'index': 'is_activity', 'width': '50px', 'formatter': 'checkbox', 'width': '75px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        hidden_module="institution", prev_module=prev_module, ppk=ppk)

@dajaxice_register    
def room_new(request, ppk):
    module = "room"
    form = forms.RoomForm(initial={'managed_by': models.Faculty.objects.get(pk=ppk)})
    form.fields["managed_by"].queryset = models.Faculty.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def room_edit(request, pk, ppk):
    module = "room"
    department = models.Room.objects.get(pk=pk)
    form = forms.RoomForm(instance=department)
    form.fields["managed_by"].queryset = models.Faculty.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def room_save(request, form, pk):
    return generic_save('room', models.Room, forms.RoomForm, pk, form)

@dajaxice_register    
def course_grid(request, ppk, gpk, prev_module):
    module = "course"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    col_names = ['Id', 'Search Key', 'Name', 'Credit', 'Level', 'Optional']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'search_key', 'index':'search_key', 'width': '100px'},
        {'name': 'name', 'index':'name', 'width': '125px'},
        {'name': 'credit', 'index':'credit', 'width': '50px', 'align': 'right'},
        {'name': 'level', 'index':'level', 'width': '50px', 'align': 'right'},
        {'name': 'is_optional', 'index':'is_optional', 'width': '50px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url,
        hidden_module="faculty", prev_module=prev_module, ppk=ppk)

@dajaxice_register    
def course_new(request, ppk):
    module = "course"
    department = models.Department.objects.get(pk=ppk)
    form = forms.CourseForm(initial={'department': department})
    form.fields['department'].queryset = models.Department.objects.filter(id=ppk)
    form.fields['prerequisite'].queryset = models.Course.objects.filter(department=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def course_edit(request, pk, ppk):
    module = "course"
    course = models.Course.objects.get(pk=pk)
    form = forms.CourseForm(instance=course)
    form.fields['department'].queryset = models.Department.objects.filter(id=ppk)
    
    return generic_form(module, form)

@dajaxice_register    
def course_save(request, form, pk):
    return generic_save('course', models.Course, forms.CourseForm, pk, form)

@dajaxice_register    
def period_grid(request, ppk, gpk, prev_module):
    module = "period"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ('enrollment', 'enrollment_statistic')
    default_module = 'enrollment_statistic'
    col_names = ['Id', 'Name', 'Start', 'End', 'Open']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'name', 'index':'name', 'width':'125px'},
        {'name': 'start_date', 'index': 'start_date', 'width':'75px', 'align':'center'},
        {'name': 'end_date', 'index': 'end_date', 'width':'75px', 'align':'center'},
        {'name': 'is_open', 'index':'is_open', 'formatter': 'checkbox', 'width':'50px', 'align':'center'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        ppk=ppk, prev_module=prev_module)

@dajaxice_register    
def period_new(request, ppk):
    module = "period"
    form = forms.PeriodForm(initial={'institution':models.Institution.objects.get(pk=ppk)})
    form.fields["institution"].queryset = models.Institution.objects.filter(id=ppk)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_start_date', 'id_end_date']})))

@dajaxice_register    
def period_edit(request, pk, ppk):
    module = "period"
    period = models.Period.objects.get(pk=pk)
    form = forms.PeriodForm(instance=period)
    form.fields["institution"].queryset = models.Institution.objects.filter(id=ppk)
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'ids':['id_start_date', 'id_end_date']})))

@dajaxice_register    
def period_save(request, form, pk):
    return generic_save('period', models.Period, forms.PeriodForm, pk, form)

@dajaxice_register    
def enrollment_grid(request, ppk, gpk, prev_module):
    module = "enrollment"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    col_names = ['Id', 'Period', 'Course', 'Student']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'period', 'index':'period', 'width':'125px'},
        {'name': 'course', 'index': 'course', 'width':'125px'},
        {'name': 'student', 'index': 'student', 'width':'125px'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        hidden_module="institution", prev_module=prev_module,ppk=ppk)

@dajaxice_register    
def enrollment_new(request, ppk):
    module = "enrollment"
    period = models.Period.objects.get(pk=ppk)
    institution = period.institution
    form = forms.EnrollmentForm(initial={'period':period})
    form.fields["period"].queryset = models.Period.objects.filter(id=ppk)
    form.fields["course"].queryset = models.Course.objects.filter(department__faculty__institution=institution)
    form.fields["student"].queryset = models.Student.objects.filter(department__faculty__institution=institution)
    
    return generic_form(module, form)

@dajaxice_register    
def enrollment_edit(request, pk, ppk):
    module = "enrollment"
    period = models.Period.objects.get(pk=ppk)
    institution = period.institution
    enrollment = models.Enrollment.objects.get(pk=pk)
    form = forms.EnrollmentForm(instance=enrollment)
    form.fields["period"].queryset = models.Period.objects.filter(id=ppk)
    form.fields["course"].queryset = models.Course.objects.filter(department__faculty__institution=institution)
    form.fields["student"].queryset = models.Student.objects.filter(department__faculty__institution=institution)
    
    return generic_form(module, form)

@dajaxice_register    
def enrollment_save(request, form, pk):
    return generic_save('enrollment', models.Enrollment, forms.EnrollmentForm, pk, form)

@dajaxice_register    
def enrollment_statistic_grid(request, ppk, gpk, prev_module):
    module = "enrollment_statistic"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ("enrollment_class","student_schedule")
    default_module = 'enrollment_class'
    col_names = ['Department', 'Course', 'Enrolled', 'Allocated']
    col_model = [
        {'name': 'student__department__name', 'index': 'student__department__name', 'width':'100px'},
        {'name': 'course__search_key', 'index': 'course__search_key', 'width':'100px'},
        {'name': 'course_count', 'index': 'course_count', 'width':'50px', 'align':'right'},
        {'name': 'allocated', 'index': 'allocated', 'width':'50px', 'align':'right'},
    ]
    url = '/json/%s/%s/' % (module, ppk)
    
    departments = models.Department.objects.filter(faculty__institution=gpk)
    values = []
    for department in departments:
        values.append({'id':department.id, 'value':department.name})
    if len(departments) > 0:
        request.session['department'] = departments[0].id
    extra_controls = [{
        'id':'department', 
        'values':values,
        'multiple':False, 
        'label':'Departments',
        'onchange':"Dajaxice.timetable.department_select(Dajax.process, {'pk':this.value})"
    }]
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        hidden_module="institution", prev_module=prev_module, ppk=ppk, 
        html="timetable/partial/navigation.administration.html",
        js="timetable/partial/navigation.administration.js",
        extra_controls=extra_controls,
        extra_buttons=[{
            "id":"allocation",
            "action":"""
                var gpk = jQuery('#enrollment_statistic_parent').attr('value');
                Dajaxice.timetable.enrollment_allocation(Dajax.process, {'gpk':gpk});
            """,
            "row_controlled":False
        },{
            "id":"clear_allocation",
            "action":"""
                var gpk = jQuery('#enrollment_statistic_parent').attr('value');
                Dajaxice.timetable.enrollment_clear(Dajax.process, {'gpk':gpk});
            """,
            "row_controlled":False
        }])

@dajaxice_register
def department_select(request, pk):
    request.session["department"] = pk
    
    dajax = Dajax()
    dajax.script("jQuery('#enrollment_statistic_table').trigger('reloadGrid');");
    dajax.script("jQuery('#enrollment_statistic > center > button.row_controlled').attr('disabled',true);")
    
    return dajax.json()

@dajaxice_register
def enrollment_allocation(request, gpk):
    department = request.session["department"]
    
    queryset = models.Enrollment.objects.filter(period=gpk)
    queryset = queryset.filter(student__department=department)
    queryset.update(lesson=None)
    
    students = set([enrollment.student for enrollment in list(queryset)])
    students = list(students)
    possible_solutions = {}
    for student in students:
        stqs = queryset.filter(student=student)
        enrollments = list(stqs)
        problem = Problem()
        registered_enrollments = []
        for enrollment in enrollments:
            classes = list(models.Class.objects.filter(course=enrollment.course))
            manually_filtered = []
            for clz in classes:
                count = models.Schedule.objects.filter(lesson=clz).count()
                if count > 0:
                    manually_filtered.append(clz)
            classes = manually_filtered
            logger.info("%s:%s" % (enrollment, classes))
            if len(classes):
                problem.addVariable(enrollment, classes)
                registered_enrollments.append(enrollment)
            else:
                logger.error("enrollment %s has no class defined" % enrollment)
        def bump(c1, c2):
            flict = not c1.conflict(c2)
            return flict
        for e1 in registered_enrollments:
            for e2 in registered_enrollments:
                if e1 != e2:
                    problem.addConstraint(bump, (e1, e2))
        solutions = problem.getSolutions()
        logger.info("solutions: %s %d" % (solutions, len(solutions)))
        possible_solutions[student] = solutions
    keys = sorted(possible_solutions, lambda a,b:len(possible_solutions[a])-len(possible_solutions[b]))
    stats = __create_class_stats__(gpk, department)
    #try one by one starting from the lowest number
    for student in keys:
        solutions = possible_solutions[student]
        chosen = None
        for solution in solutions:
            if __check_availability__(solution, stats):
                chosen = solution
                break
        if chosen == None:
            chosen = solutions[random.randint(0, len(solutions)-1)] #if no room available then we randomly choose one
        for enrollment, clz in chosen.items():
            enrollment.lesson = clz
            enrollment.save()
            stats[clz] = stats[clz]-1  
    
    dajax = Dajax()
    dajax.script("jQuery('#enrollment_statistic_table').trigger('reloadGrid')")
    return dajax.json()

def __create_class_stats__(period, department):
    stats = {}
    cqset = models.Class.objects.filter(period=period)
    cqset = cqset.filter(course__department=department)
    cqset = cqset.annotate(Count('schedule'))
    cqset = cqset.annotate(Min('schedule__room__capacity'))
    classes = list(cqset)
    for clz in classes:
        if clz.schedule__count > 0:
            stats[clz] = clz.schedule__room__capacity__min
    return stats

def __check_availability__(solution, stats):
    noflict = True
    for clz in solution.values():
        noflict = noflict and stats[clz] > 0
    return noflict

@dajaxice_register
def enrollment_clear(request, gpk):
    queryset = models.Enrollment.objects.filter(period=gpk)
    queryset.update(lesson=None)
    
    dajax = Dajax()
    dajax.script("jQuery('#enrollment_statistic_table').trigger('reloadGrid')")
    return dajax.json()

@dajaxice_register
def student_schedule_grid(request, ppk, gpk, prev_module):
    #gpk is period id, ppk is course.search_key and to be ignored
    module = "student_schedule"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    url = '/json/%s/%s/' % (module, gpk)
    
    department = request.session["department"]
    students = models.Student.objects.filter(department=department)
    values = []
    for student in students:
        values.append({'id':student.id, 'value':"%s %s" % (student.first_name, student.last_name)})
    if len(students) > 0:
        request.session["student"] = students[0].id
    extra_controls = [{
        'id':'student', 
        'values':values,
        'multiple':False, 
        'label':'Students',
        'onchange':"Dajaxice.timetable.student_select(Dajax.process, {'pk':this.value})"
    }]
    return generic_grid(crumbs, module, next_module_list, default_module, [], [], url, 
        hidden_module="period", prev_module=prev_module, ppk=ppk, 
        html="timetable/partial/navigation.administration.html",
        js="timetable/partial/schedule.administration.js",
        extra_controls=extra_controls)

@dajaxice_register
def student_select(request, pk):
    request.session["student"] = pk
    
    dajax = Dajax()
    dajax.script("jQuery('#student_schedule_content').fullCalendar('refetchEvents');");
    dajax.script("jQuery('#student_schedule > center > button.row_controlled').attr('disabled',true);")
    
    return dajax.json()
    
@dajaxice_register
def enrollment_class_grid(request, ppk, gpk, prev_module):
    #ppk is course.search_key gpk is period id
    module = "enrollment_class"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ('schedule',)
    default_module = 'schedule'
    col_names = ['Id', 'Name', 'Period', 'Course', 'Teacher']
    col_model = [
        {'name': 'id', 'index':'id', 'width': '50px', 'align': 'right'},
        {'name': 'name', 'index':'name', 'width': '50px', 'align': 'right'},
        {'name': 'period', 'index':'period', 'width':'100px'},
        {'name': 'course', 'index': 'course', 'width':'125px'},
        {'name': 'teacher', 'index': 'teacher', 'width':'100px'},
    ]
    url = '/json/%s/%s/%s/' % (module, ppk, gpk)
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url, 
        hidden_module="period", prev_module=prev_module, ppk=ppk, 
        html="timetable/partial/navigation.administration.html",
        js="timetable/partial/navigation.administration.js",
        extra_hiddens=[{'id':'period_id', 'value':gpk},],
        extra_buttons=[{
            'id':'new_enrollment_class',
            'action': """
            jQuery('#enrollment_class_selected').attr('value', -1);
            var ppk=jQuery('#enrollment_class_parent').attr('value');
            var gpk=jQuery('#period_id').attr('value');
            jQuery("#enrollment_class").append('<div id="enrollment_class_dialog">&nbsp;</div>');
            Dajaxice.timetable.enrollment_class_new(Dajax.process, {'ppk':ppk, 'gpk':gpk})""",
            'row_controlled': False},
            {'id':'edit_enrollment_class',
            'action': """
            var pk=jQuery('#enrollment_class_selected').attr('value');
            var ppk=jQuery('#enrollment_class_parent').attr('value');
            var gpk=jQuery('#period_id').attr('value');
            jQuery("#enrollment_class").append('<div id="enrollment_class_dialog">&nbsp;</div>');
            Dajaxice.timetable.enrollment_class_edit(Dajax.process, {'pk':pk, 'ppk':ppk, 'gpk':gpk})""",
            'row_controlled': True}])

@dajaxice_register    
def enrollment_class_new(request, ppk, gpk):
    #ppk is course search_key and gpk is period id
    module = "enrollment_class"
    period = models.Period.objects.get(pk=gpk)
    course = models.Course.objects.get(search_key=ppk)
    institution = period.institution
    form = forms.ClassForm(initial={'period':period, 'course': course})
    form.fields["period"].queryset = models.Period.objects.filter(id=gpk)
    form.fields["course"].queryset = models.Course.objects.filter(department__faculty__institution=institution).filter(search_key=ppk)
    form.fields["teacher"].queryset = models.Employee.objects.filter(institution=institution).filter(is_teacher=True)
    
    return generic_form(module, form)

@dajaxice_register    
def enrollment_class_edit(request, pk, ppk, gpk):
    module = "enrollment_class"
    period = models.Period.objects.get(pk=gpk)
    institution = period.institution
    clazz = models.Class.objects.get(pk=pk)
    form = forms.ClassForm(instance=clazz)
    form.fields["period"].queryset = models.Period.objects.filter(id=gpk)
    form.fields["course"].queryset = models.Course.objects.filter(department__faculty__institution=institution).filter(search_key=ppk)
    form.fields["teacher"].queryset = models.Employee.objects.filter(institution=institution).filter(is_teacher=True)
    
    return generic_form(module, form)

@dajaxice_register    
def enrollment_class_save(request, form, pk):
    return generic_save('enrollment_class', models.Class, forms.ClassForm, pk, form)

@dajaxice_register
def schedule_grid(request, ppk, gpk, prev_module):
    #ppk is class
    module = "schedule"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    url = '/json/%s/%s/' % (module, ppk)
    clazz = models.Class.objects.get(pk=ppk)
    faculty = clazz.course.department.faculty
    rooms = faculty.room_set.filter(is_classroom=True).order_by('search_key')
    values = []
    for room in rooms:
        values.append({'id':room.id, 'value':room.search_key,})
    if len(rooms) > 0:
        request.session["room"] = rooms[0].id
    extra_controls = [{
        'id':'room', 
        'values':values,
        'multiple':False, 
        'label':'Rooms',
        'onchange':"Dajaxice.timetable.room_select(Dajax.process, {'pk':this.value})"
    }]
    
    return generic_grid(crumbs, module, next_module_list, default_module, [], [], url, 
        hidden_module="enrollment_statistic", prev_module=prev_module, ppk=ppk, 
        html="timetable/partial/navigation.administration.html",
        js="timetable/partial/schedule.administration.js",
        extra_controls=extra_controls,
        extra_buttons=[
            {'id':'add_schedule', 'action':"""
                jQuery('schedule > button.row_controlled').attr('disabled', true);
                jQuery('schedule_selected').attr('value', -1);
                var ppk=jQuery('#schedule_parent').attr('value');
                jQuery("#schedule").append('<div id="schedule_dialog">&nbsp;</div>');
                Dajaxice.timetable.schedule_new(Dajax.process, {'ppk':ppk});
            """},
            {'id':'edit_schedule', 'action':"""
                var pk=jQuery('#schedule_selected').attr('value');
                var ppk=jQuery('#schedule_parent').attr('value');
                jQuery("#schedule").append('<div id="schedule_dialog">&nbsp;</div>');
                Dajaxice.timetable.schedule_edit(Dajax.process, {'pk':pk,'ppk':ppk});
            """, 'row_controlled':True},
            {'id':'delete_schedule', 'action':"""
                var pk=jQuery('#schedule_selected').attr('value');
                jQuery('schedule > button.row_controlled').attr('disabled',true);
                Dajaxice.timetable.schedule_delete(Dajax.process, {'pk':pk});
                jQuery('schedule_selected').attr('value', -1);
            """, 'row_controlled':True}
            ])

@dajaxice_register
def schedule_new(request, ppk):
    module = "schedule"
    clazz = models.Class.objects.get(pk=ppk)
    room = models.Room.objects.get(pk=request.session["room"])
    faculty = clazz.course.department.faculty
    rooms = faculty.room_set.filter(is_classroom=True).order_by('search_key')
    start_time = time(6).strftime("%H:%M")
    end_time = time(6 + clazz.course.credit).strftime("%H:%M")
    form = forms.ScheduleForm(initial={'lesson':clazz, 'room':room, 'start_time':start_time, 'end_time':end_time})
    form.fields["lesson"].queryset = models.Class.objects.filter(id=ppk)
    form.fields["room"].queryset = rooms
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'start_time':'id_start_time', 'end_time':'id_end_time'})))

@dajaxice_register
def schedule_edit(request, pk, ppk):
    module = "schedule"
    schedule = models.Schedule.objects.get(pk=pk)
    clazz = models.Class.objects.get(pk=ppk)
    faculty = clazz.course.department.faculty
    rooms = faculty.room_set.filter(is_classroom=True).order_by('search_key')
    form = forms.ScheduleForm(instance=schedule)
    form.fields["lesson"].queryset = models.Class.objects.filter(id=ppk)
    form.fields["room"].queryset = rooms
    after_load = get_template("timetable/partial/dialog.afterload.calendar.js")
    
    return generic_form(module, form, after_load=after_load.render(Context({'start_time':'id_start_time', 'end_time':'id_end_time'})))

@dajaxice_register    
def schedule_save(request, form, pk):
    return generic_save('schedule', models.Schedule, forms.ScheduleForm, pk, form,
        after_save_script="jQuery('#schedule_content').fullCalendar('refetchEvents')")

@dajaxice_register
def schedule_delete(request, pk):
    schedule = models.Schedule.objects.get(pk=pk)
    schedule.delete()
    
    dajax = Dajax()
    dajax.script("jQuery('#schedule_table').fullCalendar('refetchEvents')")
    
    return dajax.json()

@dajaxice_register    
def schedule_drop(request, pk, day, minute):
    schedule = models.Schedule.objects.get(pk=pk)
    schedule.day += day
    delta = timedelta(minutes = minute)
    schedule.start_time = (datetime.combine(date.today(), schedule.start_time) + delta).time()
    schedule.end_time = (datetime.combine(date.today(), schedule.end_time) + delta).time()
    schedule.save()

    return Dajax().json();


@dajaxice_register    
def schedule_resize(request, pk, minute):
    schedule = models.Schedule.objects.get(pk=pk)
    delta = timedelta(minutes = minute)
    schedule.end_time = (datetime.combine(date.today(), schedule.end_time) + delta).time()
    schedule.save()

    return Dajax().json();

@dajaxice_register
def room_select(request, pk):
    logger.info("current room id: %d" % request.session["room"])
    room = models.Room.objects.get(pk=pk)
    request.session["room"] = room.id
    logger.info("new room id: %d" % request.session["room"])
    
    dajax = Dajax()
    dajax.script("jQuery('#schedule_content').fullCalendar('refetchEvents');");
    dajax.script("jQuery('button.row_controlled').attr('disabled',true);")
    
    return dajax.json()

@dajaxice_register    
def course_tree_grid(request, ppk, gpk, prev_module):
    module = "course_tree"
    crumbs = breadcrumbs(request.session, prev_module, module)
    next_module_list = ()
    default_module = None
    col_names = []
    col_model = []
    url = '/json/course_tree/%s/' % ppk
    
    return generic_grid(crumbs, module, next_module_list, default_module, col_names, col_model, url,
        hidden_module="faculty", prev_module=prev_module, ppk=ppk,
        html="timetable/partial/navigation.administration.html",
        js="timetable/partial/course_tree.administration.js")

@dajaxice_register
def course_drop(request, target, source, hitmode):
    source = models.Course.objects.get(pk=source)
    target = models.Course.objects.get(pk=target)
    if (hitmode == "over"):
        source.prerequisite = target
        source.level = target.level + 1
    else:
        source.prerequisite = target.prerequisite
        source.level = target.level
    source.save()
    dajax = Dajax()
    return dajax.json()

@dajaxice_register
def course_level_grid(request, ppk, gpk, prev_module):
    module = "course_level"
    crumbs = breadcrumbs(request.session, prev_module, module)
    max_level = models.Course.objects.filter(department=ppk).aggregate(Max("level"))["level__max"]
    t = get_template("timetable/partial/course_level.administration.html")
    html = t.render(Context({
        'module': module, 
        'ppk':ppk,
        'max_level':max_level}))
    t = get_template("timetable/partial/course_level.administration.js")
    js = t.render(Context({'max_level':max_level, 'ppk':ppk}))
    
    t = get_template('timetable/partial/crumbs.administration.html')
    crumbshtml = t.render(Context({'crumbs': crumbs}))
    
    dajax = Dajax()
    dajax.assign("#%s" % module, "innerHTML", html)
    dajax.script(js)
    dajax.assign("#breadcrumbs", "innerHTML", crumbshtml)
    
    return dajax.json()

@dajaxice_register
def course_level_add(request, level):
    t = get_template("timetable/partial/course_level.box.html")
    part = t.render(Context({'level':level}))
    t = get_template("timetable/partial/course_level.content.js")
    js = t.render(Context())
    
    dajax = Dajax()
    dajax.append("#level_container", "innerHTML", part)
    dajax.assign("#max_level", "value", level)
    dajax.script(js)
    return dajax.json()

@dajaxice_register
def course_level_update(request, level, ppk):
    department = models.Department.objects.get(pk=ppk)
    courses = department.course_set.filter(level=level)
    t = get_template("timetable/partial/course_level.content.html")
    part = t.render(Context({'level':level, 'courses':courses}))
    t = get_template("timetable/partial/course_level.content.js")
    js = t.render(Context())
    
    dajax = Dajax()
    dajax.assign("#level_%s" % level, "innerHTML", part)
    dajax.script(js)
    return dajax.json();

@dajaxice_register
def course_level_drag(request, source, level):
    source = models.Course.objects.get(pk=source)
    level = int(level) #target level
    
    def level_update(course, level):
        previous_level = course.level
        course.level = level
        course.save()
        if level > previous_level:
            course_below = models.Course.objects.filter(prerequisite=course)
            for prereq in course_below:
                level_update(prereq, level + 1)
        
    level_update(source, level)
    department = source.department
    max_level = department.course_set.aggregate(Max('level'))['level__max']
    
    return simplejson.dumps({'max_level':max_level})
