from django.db import models

class CommonData(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField(max_length=120)
    city = models.CharField(max_length=60)
    province = models.CharField(max_length=30)
    country = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=20)
    fax = models.CharField(max_length=20, blank=True)

    class Meta:
        abstract = True

class Institution(CommonData):
    def __unicode__(self):
        return "%s - %s" % (self.name, self.city)

class Faculty(CommonData):
    institution = models.ForeignKey(Institution)
    
    def __unicode__(self):
        return "%s" % self.name

class Department(models.Model):
    name = models.CharField(max_length=100)
    faculty = models.ForeignKey(Faculty)
    
    def __unicode__(self):
        return "%s" % self.name

GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)

class CommonInfo(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateField()
    birth_city = models.CharField(max_length=30)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    address = models.TextField(max_length=120)
    city = models.CharField(max_length=60)
    phone = models.CharField(max_length=20, blank=True)
    mobile = models.CharField(max_length=20, blank=True)
    email = models.EmailField(blank=True)

    class Meta:
        abstract = True

class Rank(models.Model):
    name = models.CharField(max_length=20)
    institution = models.ForeignKey(Institution)
    
    def __unicode__(self):
        return self.name

class Employee(CommonInfo):
    photo = models.ImageField(upload_to='photo/', blank=True)
    is_teacher = models.BooleanField()
    institution = models.ForeignKey(Institution)
    rank = models.ForeignKey(Rank, null=True)
    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)

class Student(CommonInfo):
    register = models.CharField(max_length=20, unique=True)
    photo = models.ImageField(upload_to='photo/', blank=True)
    department = models.ForeignKey(Department)
    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)

class Guardian(CommonInfo):
    student = models.ForeignKey(Student)
    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)

class Room(models.Model):
    search_key = models.CharField(max_length=10)
    description = models.TextField(blank=True)
    capacity = models.PositiveIntegerField()
    is_activity = models.BooleanField()
    is_classroom = models.BooleanField()
    managed_by = models.ForeignKey(Faculty)
    def __unicode__(self):
        return "%s" % (self.search_key)

class Course(models.Model):
    search_key = models.CharField(max_length=10)
    name = models.CharField(max_length=30)
    prerequisite = models.ForeignKey('self', null=True, blank=True)
    credit = models.PositiveSmallIntegerField(default=2)
    level = models.PositiveSmallIntegerField(default=1)
    is_optional = models.BooleanField(default=False)
    department = models.ForeignKey(Department, null=True) #nullable to satisfy migration
    
    def __unicode__(self):
        return "%s %s" % (self.search_key, self.name)

class Period(models.Model):
    name = models.CharField(max_length=20, default='No Name')
    start_date = models.DateField()
    end_date = models.DateField()
    is_open = models.BooleanField()
    institution = models.ForeignKey(Institution, null=True) #nullable to satisfy migration
    def __unicode__(self):
        return "%s" % (self.name)

class Class(models.Model):
    period = models.ForeignKey(Period, null=True) #nullable to satisfy migration
    course = models.ForeignKey(Course)
    teacher = models.ForeignKey(Employee)
    name = models.CharField(default="A", max_length=2)
    
    def __unicode__(self):
        return "%s %s" % (self.course, self.name)
    
    def conflict(self, other_class):
        flict = False
        for schedule1 in self.schedule_set.all():
            for schedule2 in other_class.schedule_set.all():
                flict = flict or schedule1.conflict(schedule2)
                
        return flict

DAY_CHOICES = (
    (0, 'Sunday'),
    (1, 'Monday'),
    (2, 'Tuesday'),
    (3, 'Wednesday'),
    (4, 'Thursday'),
    (5, 'Friday'),
    (6, 'Saturday'),
)

class Schedule(models.Model):
    day = models.SmallIntegerField(choices=DAY_CHOICES)
    start_time = models.TimeField()
    end_time = models.TimeField()
    lesson = models.ForeignKey(Class)
    room = models.ForeignKey(Room)
    
    def __unicode__(self):
        return "%s %s %s" % (self.day, self.start_time, self.end_time)
    
    def conflict(self, other_schedule):
        flict = self.day == other_schedule.day
        if self.start_time < other_schedule.start_time:
            flict = flict and other_schedule.start_time < self.end_time
        else:
            flict = flict and self.start_time < other_schedule.end_time
        
        return flict 

class ReplacementSchedule(models.Model):
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    room = models.ForeignKey(Room)
    replacement_for = models.ForeignKey(Schedule)

class Enrollment(models.Model):
    period = models.ForeignKey(Period)
    student = models.ForeignKey(Student)
    course = models.ForeignKey(Course)
    lesson = models.ForeignKey(Class, null=True, blank=True)
    
    class Meta:
        unique_together = ('period', 'student', 'course',)
    
    def __unicode__(self):
        return "%s %s" % (self.student.first_name, self.course.search_key)

