from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from ankimo import settings
from dajaxice.core import dajaxice_autodiscover
from timetable import views, json

dajaxice_autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ankimo.views.home', name='home'),
    # url(r'^ankimo/', include('ankimo.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    (r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
    (r'^$', TemplateView.as_view(template_name="index.html")),
    (r'^administration/$', TemplateView.as_view(template_name="timetable/administration.html")),
    (r'^hello/$', views.hello),
    (r'^json/institution/$', json.institution_all),
    (r'^json/faculty/(\d{1,3})/$', json.faculty_all),
    (r'^json/department/(\d{1,4})/$', json.department_all),
    (r'^json/rank/(\d{1,2})/$', json.rank_all),
    (r'^json/employee/(\d{1,4})/$', json.employee_all),
    (r'^json/student/(\d{1,10})/$', json.student_all),
    (r'^json/guardian/(\d{1,10})/$', json.guardian_all),
    (r'^json/room/(\d{1,5})/$', json.room_all),
    (r'^json/course/(\d{1,5})/$', json.course_all),
    (r'^json/period/(\d{1,5})/$', json.period_all),
    (r'^json/enrollment/(\d{1,5})/$', json.enrollment_all),
    (r'^json/enrollment_statistic/(\d{1,5})/$', json.enrollment_statistic_all),
    (r'^json/enrollment_class/([A-Za-z]+[\-_]?\d{1,5})/(\d{1,5})/$', json.enrollment_class_all),
    (r'^json/schedule/(\d{1,5})/$', json.schedule_all),
    (r'^json/course_tree/(\d{1,5})/$', json.course_tree),
    (r'^json/student_schedule/(\d{1,5})/$', json.student_schedule_all),
)
